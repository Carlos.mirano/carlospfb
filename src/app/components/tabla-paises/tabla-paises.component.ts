import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Pais } from '../../interfaces/pais';

@Component({
  selector: 'app-tabla-paises',
  templateUrl: './tabla-paises.component.html',
  styleUrls: ['./tabla-paises.component.css']
})
export class TablaPaisesComponent implements OnInit {

  paisModal: any = [];
  paises: Pais[];
  constructor(private apiService: ApiService) { }

  imprime(item) {
    this.paisModal = item;
  }

  ngOnInit() {
    this.apiService.getPais().subscribe((data) => {
      this.paises = data as Pais[];
    })
  }

}
