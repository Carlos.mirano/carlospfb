import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TablaPaisesComponent } from './components/tabla-paises/tabla-paises.component';

const routes: Routes = [
  { path: 'paises', component: TablaPaisesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
